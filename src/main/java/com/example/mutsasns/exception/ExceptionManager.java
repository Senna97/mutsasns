package com.example.mutsasns.exception;

import com.example.mutsasns.domain.ErrorResponse;
import com.example.mutsasns.domain.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;

@RestControllerAdvice
public class ExceptionManager {
    @ExceptionHandler(MutsaSNSException.class)
    public ResponseEntity<?> mutsaSNSExceptionHandler(MutsaSNSException mutsaSNSException) {
        ErrorResponse errorResponse = new ErrorResponse(mutsaSNSException.getErrorCode(), mutsaSNSException.getErrorCode().getMessage());
        return ResponseEntity.status(mutsaSNSException.getErrorCode().getHttpStatus())
                .body(Response.error(errorResponse));
    }

    @ExceptionHandler(SQLException.class)
    public ResponseEntity<?> sqlExceptionHandler(SQLException sqlException) {
        ErrorResponse errorResponse = new ErrorResponse(ErrorCode.DATABASE_ERROR, sqlException.getMessage());
        return ResponseEntity.status(ErrorCode.DATABASE_ERROR.getHttpStatus())
                .body(Response.error(errorResponse));
    }
}
