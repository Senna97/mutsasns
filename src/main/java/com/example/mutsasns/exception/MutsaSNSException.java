package com.example.mutsasns.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MutsaSNSException extends RuntimeException{
    private ErrorCode errorCode;
}
