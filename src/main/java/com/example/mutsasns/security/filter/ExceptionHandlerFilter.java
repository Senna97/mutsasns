package com.example.mutsasns.security.filter;

import com.example.mutsasns.domain.ErrorResponse;
import com.example.mutsasns.domain.Response;
import com.example.mutsasns.exception.ErrorCode;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class ExceptionHandlerFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        try {
            filterChain.doFilter(request, response);
        } catch (PrematureJwtException e) {
            log.error("접근이 허용되지 않은 토큰입니다.");
        } catch (ExpiredJwtException e) {
            log.error("유효시간이 만료된 토큰입니다.");
        } catch (UnsupportedJwtException e) {
            log.error("형식이 잘못된 토큰입니다.");
        } catch (MalformedJwtException e) {
            log.error("구조적인 문제가 있는 토큰입니다.");
            try {
                setResponse(response, ErrorCode.INVALID_TOKEN, "구조적인 문제가 있는 토큰입니다.");
            } catch (JSONException ex) {
                throw new RuntimeException(ex);
            }
        } catch (SignatureException e) {
            log.error("시그니처 검증에 실패한 토큰입니다.");
        } catch (ClaimJwtException e) {
            log.error("권한 claim 검사에 실패한 토큰입니다.");
        } catch (NullPointerException e) {

            filterChain.doFilter(request, response);
        }
    }

    private void setResponse(HttpServletResponse response, ErrorCode errorCode, String message) throws IOException, JSONException {
        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        JSONObject responseJson = new JSONObject();
        JSONObject result = new JSONObject();

        ErrorResponse errorResponse = new ErrorResponse(errorCode, message);
        result.put("errorCode", errorResponse.getErrorCode());
        result.put("message", errorResponse.getMessage());

        Response returnResponse = new Response("ERROR", result);
        responseJson.put("resultCode", returnResponse.getResultCode());
        responseJson.put("result", returnResponse.getResult());

        response.getWriter().print(responseJson);
    }
}