package com.example.mutsasns.domain;

public enum Role {
    USER, ADMIN
}