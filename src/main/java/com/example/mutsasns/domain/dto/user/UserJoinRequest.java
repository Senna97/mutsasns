package com.example.mutsasns.domain.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserJoinRequest {
    private Long userId;
    private String userName;
    private String password;

    public UserJoinRequest(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }
}
