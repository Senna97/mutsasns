package com.example.mutsasns.domain.dto.post;

import com.example.mutsasns.domain.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MyFeedResponse {
    private Long id;
    private String title;
    private String body;
    private String userName;
    private String createdAt;

    public static MyFeedResponse of(Post post) {
        return MyFeedResponse.builder()
                .id(post.getPostId())
                .title(post.getTitle())
                .body(post.getBody())
                .userName(post.getUser().getUserName())
                .createdAt(post.getCreatedAt())
                .build();
    }
}

