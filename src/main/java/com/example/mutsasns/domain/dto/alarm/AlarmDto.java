package com.example.mutsasns.domain.dto.alarm;

import com.example.mutsasns.domain.AlarmType;
import com.example.mutsasns.domain.entity.Alarm;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AlarmDto {
    private Long id;
    private AlarmType alarmType;
    private Long fromUserId;
    private Long targetId;
    private String text;
    private String createdAt;

    public static AlarmDto of(Alarm alarm) {
        return AlarmDto.builder()
                .id(alarm.getId())
                .alarmType(alarm.getAlarmType())
                .fromUserId(alarm.getFromUserId())
                .targetId(alarm.getTargetId())
                .text(alarm.getText())
                .createdAt(alarm.getCreatedAt())
                .build();
    }
}