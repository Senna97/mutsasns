package com.example.mutsasns.domain.dto.post;

import com.example.mutsasns.domain.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {
    private Long id;
    private String title;
    private String body;
    private String userName;
    private String createdAt;
    private String lastModifiedAt;

    public static Page<PostDto> toPostList(Page<Post> posts) {
        return posts.map(postMap -> PostDto.builder()
                .id(postMap.getPostId())
                .title(postMap.getTitle())
                .body(postMap.getBody())
                .userName(postMap.getUser().getUserName())
                .createdAt(postMap.getCreatedAt())
                .lastModifiedAt(postMap.getLastModifiedAt())
                .build());
    }

    public static PostDto toPost(Post post) {
        return PostDto.builder()
                .id(post.getPostId())
                .title(post.getTitle())
                .body(post.getBody())
                .userName(post.getUser().getUserName())
                .createdAt(post.getCreatedAt())
                .lastModifiedAt(post.getLastModifiedAt())
                .build();
    }
}

