package com.example.mutsasns.controller;

import com.example.mutsasns.domain.Response;
import com.example.mutsasns.service.LikesService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
public class LikesController {

    private final LikesService likesService;

    @PostMapping("/{postId}/likes")
    @ApiOperation(value="좋아요 누르기", notes="‘좋아요’는 한 번만 누를 수 있습니다.")
    public Response<String> doLikes(@PathVariable Long postId, Authentication authentication) {
        String userName = authentication.getName();
        String result = likesService.doLikes(postId, userName);
        return Response.success(result);
    }
    @GetMapping("/{postId}/likes")
    @ApiOperation(value="좋아요 조회", notes="특정 글에 대한 ‘좋아요’ 개수를 조회합니다.")
    public Response<Integer> countLikes(@PathVariable Long postId) {
        Integer result = likesService.countLikes(postId);
        return Response.success(result);
    }
}