package com.example.mutsasns.controller;

import com.example.mutsasns.domain.Response;
import com.example.mutsasns.domain.dto.user.UserJoinRequest;
import com.example.mutsasns.domain.dto.user.UserJoinResponse;
import com.example.mutsasns.domain.dto.user.UserLoginRequest;
import com.example.mutsasns.domain.dto.user.UserLoginResponse;
import com.example.mutsasns.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/join")
    @ApiOperation(value="회원가입", notes="아이디와 비밀번호를 입력하여 회원가입을 할 수 있습니다.")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest userJoinRequest) {
        UserJoinResponse userJoinResponse = userService.join(userJoinRequest.getUserName(), userJoinRequest.getPassword());
        return Response.success(userJoinResponse);
    }

    @PostMapping("/login")
    @ApiOperation(value="로그인", notes="로그인 성공 시 token 을 리턴합니다.")
    public Response<UserLoginResponse> login(@RequestBody UserLoginRequest userLoginRequest) {
        String token = userService.login(userLoginRequest.getUserName(), userLoginRequest.getPassword());
        return Response.success(new UserLoginResponse(token));
    }
}