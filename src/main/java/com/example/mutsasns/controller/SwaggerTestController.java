package com.example.mutsasns.controller;

import com.example.mutsasns.service.SwaggerTestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hello")
@RequiredArgsConstructor
public class SwaggerTestController {

    private final SwaggerTestService swaggerTestService;

    @GetMapping
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok().body("장서현");
    }

    @GetMapping("/{num}")
    public String sumOfDigit(@PathVariable int num) {
        return swaggerTestService.sumOfDigit(num);
    }
}