package com.example.mutsasns.controller;

import com.example.mutsasns.domain.Response;
import com.example.mutsasns.domain.dto.comment.CommentDeleteResponse;
import com.example.mutsasns.domain.dto.comment.CommentRequest;
import com.example.mutsasns.domain.dto.comment.CommentDto;
import com.example.mutsasns.domain.dto.comment.CommentUpdateResponse;
import com.example.mutsasns.service.CommentService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @PostMapping("/{postId}/comments")
    @ApiOperation(value="댓글 작성", notes="로그인한 사람만 댓글을 작성할 수 있습니다.")
    public Response<CommentDto> create(@PathVariable Long postId, Authentication authentication, @RequestBody CommentRequest commentRequest) {
        String userName = authentication.getName();
        CommentDto commentDto = commentService.create(postId, userName, commentRequest);
        return Response.success(commentDto);
    }

    @GetMapping("/{postId}/comments")
    @ApiOperation(value="댓글 조회", notes="특정 글에 대한 댓글을 조회합니다.")
    public Response<Page<CommentDto>> read(@PageableDefault(sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable, @PathVariable Long postId) {
        Page<CommentDto> commentList = commentService.read(pageable, postId);
        return Response.success(commentList);
    }

    @PutMapping("/{postId}/comments/{id}")
    @ApiOperation(value="댓글 수정", notes="댓글을 작성한 사람만 댓글을 수정할 수 있습니다.")
    public Response<CommentUpdateResponse> update(@PathVariable Long postId, @PathVariable Long id, Authentication authentication, @RequestBody CommentRequest commentRequest) {
        String userName = authentication.getName();
        CommentUpdateResponse updateResponse = commentService.update(postId, id, userName, commentRequest);
        return Response.success(updateResponse);
    }

    @DeleteMapping("/{postId}/comments/{id}")
    @ApiOperation(value="댓글 삭제", notes="댓글을 작성한 사람만 댓글을 삭제할 수 있습니다.")
    public Response<CommentDeleteResponse> delete(@PathVariable Long postId, @PathVariable Long id, Authentication authentication) {
        String userName = authentication.getName();
        CommentDto commentDto = commentService.delete(postId, id, userName);
        return Response.success(new CommentDeleteResponse("댓글 삭제 완료", commentDto.getId()));
    }
}