package com.example.mutsasns.controller;

import com.example.mutsasns.domain.Response;
import com.example.mutsasns.domain.dto.alarm.AlarmDto;
import com.example.mutsasns.service.AlarmService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/alarms")
@RequiredArgsConstructor
public class AlarmController {

    private final AlarmService alarmService;

    @GetMapping
    @ApiOperation(value="알림 리스트 조회", notes="특정 사용자의 글에 대한 알림(댓글, 좋아요)을 조회합니다.")
    public Response<Page<AlarmDto>> alarm(@PageableDefault(size = 20, sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable, Authentication authentication) {
        String userName = authentication.getName();
        Page<AlarmDto> alarmList = alarmService.alarm(pageable, userName);
        return Response.success(alarmList);
    }
}