package com.example.mutsasns.controller;

import com.example.mutsasns.domain.Response;
import com.example.mutsasns.domain.dto.post.MyFeedResponse;
import com.example.mutsasns.domain.dto.post.PostDto;
import com.example.mutsasns.domain.dto.post.PostRequest;
import com.example.mutsasns.domain.dto.post.PostResponse;
import com.example.mutsasns.service.PostService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;

    @PostMapping
    @ApiOperation(value="포스트 작성", notes="로그인한 사람만 포스트를 작성할 수 있습니다.")
    public Response<PostResponse> create(@RequestBody PostRequest postRequest, Authentication authentication) {
        String userName = authentication.getName();
        PostDto postDto = postService.create(postRequest, userName);
        return Response.success(new PostResponse("포스트 등록 완료", postDto.getId()));
    }

    @GetMapping
    @ApiOperation(value="포스트 리스트 조회", notes="작성된 모든 포스트를 조회합니다.")
    public Response<Page<PostDto>> readAll(@PageableDefault(size = 20, sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable) {
        Page<PostDto> list = postService.readAll(pageable);
        return Response.success(list);
    }

    @GetMapping("/{id}")
    @ApiOperation(value="포스트 상세 조회", notes="특정 포스트를 상세 조회합니다.")
    public Response<PostDto> readById(@PathVariable Long id) {
        PostDto postDto = postService.readById(id);
        return Response.success(postDto);
    }

    @PutMapping("/{id}")
    @ApiOperation(value="포스트 수정", notes="포스트를 작성한 사람만 포스트를 수정할 수 있습니다.")
    public Response<PostResponse> update(@PathVariable Long id, @RequestBody PostRequest postRequest, Authentication authentication) {
        String userName = authentication.getName();
        PostDto postDto = postService.update(id, postRequest, userName);
        return Response.success(new PostResponse("포스트 수정 완료", postDto.getId()));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value="포스트 삭제", notes="포스트를 작성한 사람만 포스트를 삭제할 수 있습니다.")
    public Response<PostResponse> delete(@PathVariable Long id, Authentication authentication) {
        String userName = authentication.getName();
        PostDto postDto = postService.delete(id, userName);
        return Response.success(new PostResponse("포스트 삭제 완료", postDto.getId()));
    }

    @GetMapping("/my")
    @ApiOperation(value="마이 피드 조회", notes="특정 사용자가 작성한 모든 글을 조회합니다.")
    public Response<Page<MyFeedResponse>> myFeed(@PageableDefault(size = 20, sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable, Authentication authentication) {
        String userName = authentication.getName();
        Page<MyFeedResponse> myFeed = postService.myFeed(pageable, userName);
        return Response.success(myFeed);
    }
}