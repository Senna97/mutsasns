package com.example.mutsasns.service;

import com.example.mutsasns.domain.AlarmType;
import com.example.mutsasns.domain.entity.Alarm;
import com.example.mutsasns.domain.entity.Likes;
import com.example.mutsasns.domain.entity.Post;
import com.example.mutsasns.domain.entity.User;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.repository.AlarmRepository;
import com.example.mutsasns.repository.LikesRepository;
import com.example.mutsasns.repository.PostRepository;
import com.example.mutsasns.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LikesService {

    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final LikesRepository likesRepository;
    private final AlarmRepository alarmRepository;

    public String doLikes(Long postId, String userName) {

        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.USERNAME_NOT_FOUND));

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        likesRepository.findByPostAndUser(post, user)
                .ifPresent(likes -> {
                    throw new MutsaSNSException(ErrorCode.DUPLICATED_LIKES);
                });

        Likes likes = Likes.of(user, post);
        likesRepository.save(likes);
        alarmRepository.save(new Alarm(post.getUser(), AlarmType.NEW_LIKE_ON_POST, user.getUserId(), postId, "new like!"));
        return "좋아요를 눌렀습니다.";
    }

    public Integer countLikes(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.POST_NOT_FOUND));
        return likesRepository.countByPost(post);
    }
}