package com.example.mutsasns.service;

import com.example.mutsasns.domain.dto.post.MyFeedResponse;
import com.example.mutsasns.domain.dto.post.PostDto;
import com.example.mutsasns.domain.dto.post.PostRequest;
import com.example.mutsasns.domain.entity.Post;
import com.example.mutsasns.domain.entity.User;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.repository.PostRepository;
import com.example.mutsasns.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class PostService {

    private final UserRepository userRepository;

    private final PostRepository postRepository;

    public PostDto create(PostRequest postRequest, String userName) {

        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.INVALID_PERMISSION));

        Post savedPost = postRepository.save(Post.of(postRequest.getTitle(), postRequest.getBody(), user));

        PostDto postDto = PostDto.builder()
                .id(savedPost.getPostId())
                .build();

        return postDto;
    }

    public Page<PostDto> readAll(Pageable pageable) {

        Page<Post> posts = postRepository.findAll(pageable);
        Page<PostDto> list = PostDto.toPostList(posts);
        return list;
    }

    public PostDto readById(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.POST_NOT_FOUND));
        PostDto postDto = PostDto.toPost(post);
        return postDto;
    }

    @Transactional
    public PostDto update(Long id, PostRequest postRequest, String userName) {

        Post post = postRepository.findById(id)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.USERNAME_NOT_FOUND));

        if (!Objects.equals(post.getUser().getUserName(), user.getUserName())) {
            throw new MutsaSNSException(ErrorCode.INVALID_PERMISSION);
        }

        post.setTitle(postRequest.getTitle());
        post.setBody(postRequest.getBody());
        Post updatedPost = postRepository.saveAndFlush(post);

        PostDto postDto = PostDto.builder()
                .id(updatedPost.getPostId())
                .build();

        return postDto;
    }

    public PostDto delete(Long id, String userName) {

        Post post = postRepository.findById(id)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.USERNAME_NOT_FOUND));

        if (!Objects.equals(post.getUser().getUserName(), user.getUserName())) {
            throw new MutsaSNSException(ErrorCode.INVALID_PERMISSION);
        }

        postRepository.deleteById(id);

        PostDto postDto = PostDto.builder()
                .id(post.getPostId())
                .build();

        return postDto;
    }

    public Page<MyFeedResponse> myFeed(Pageable pageable, String userName) {

        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.USERNAME_NOT_FOUND));

        return postRepository.findAllByUser(pageable, user).map(MyFeedResponse::of);
    }
}