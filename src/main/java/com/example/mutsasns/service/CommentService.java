package com.example.mutsasns.service;

import com.example.mutsasns.domain.AlarmType;
import com.example.mutsasns.domain.dto.comment.CommentDto;
import com.example.mutsasns.domain.dto.comment.CommentRequest;
import com.example.mutsasns.domain.dto.comment.CommentUpdateResponse;
import com.example.mutsasns.domain.entity.Alarm;
import com.example.mutsasns.domain.entity.Comment;
import com.example.mutsasns.domain.entity.Post;
import com.example.mutsasns.domain.entity.User;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.repository.AlarmRepository;
import com.example.mutsasns.repository.CommentRepository;
import com.example.mutsasns.repository.PostRepository;
import com.example.mutsasns.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final AlarmRepository alarmRepository;

    public CommentDto create(Long postId, String userName, CommentRequest commentRequest) {

        // 로그인 하지 않은 경우 실패
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.USERNAME_NOT_FOUND));

        // 게시물이 존재하지 않는 경우 실패
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        Comment comment = Comment.builder()
                .comment(commentRequest.getComment())
                .user(user)
                .post(post)
                .build();

        Comment savedComment = commentRepository.save(comment);
        alarmRepository.save(new Alarm(post.getUser(), AlarmType.NEW_COMMENT_ON_POST, user.getUserId(), postId, "new comment!"));
        return CommentDto.of(savedComment);
    }

    public Page<CommentDto> read(Pageable pageable, Long postId) {

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        return commentRepository.findAllByPost(pageable, post).map(CommentDto::of);
    }

    public CommentUpdateResponse update(Long postId, Long id, String userName, CommentRequest commentRequest) {

        // 인증 실패
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.USERNAME_NOT_FOUND));

        // Post 없는 경우 실패
        postRepository.findById(postId)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.COMMENT_NOT_FOUND));

        // 작성자 불일치
        if (!Objects.equals(comment.getUser().getUserName(), user.getUserName())) {
            throw new MutsaSNSException(ErrorCode.INVALID_PERMISSION);
        }

        comment.setComment(commentRequest.getComment());
        Comment modifiedComment = commentRepository.saveAndFlush(comment);

        return CommentUpdateResponse.builder()
                .id(modifiedComment.getCommentId())
                .comment(modifiedComment.getComment())
                .userName(modifiedComment.getUser().getUserName())
                .postId(modifiedComment.getPost().getPostId())
                .createdAt(modifiedComment.getCreatedAt())
                .lastModifiedAt(modifiedComment.getLastModifiedAt())
                .build();
    }

    public CommentDto delete(Long postId, Long id, String userName) {

        // 인증 실패
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.USERNAME_NOT_FOUND));

        // Post 없는 경우 실패
        postRepository.findById(postId)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.COMMENT_NOT_FOUND));

        // 작성자 불일치
        if (!Objects.equals(comment.getUser().getUserName(), user.getUserName())) {
            throw new MutsaSNSException(ErrorCode.INVALID_PERMISSION);
        }

        commentRepository.delete(comment);

        return CommentDto.builder()
                .id(id)
                .build();
    }
}