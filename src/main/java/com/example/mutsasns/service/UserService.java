package com.example.mutsasns.service;

import com.example.mutsasns.domain.dto.user.UserJoinResponse;
import com.example.mutsasns.domain.entity.User;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.repository.UserRepository;
import com.example.mutsasns.security.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")
    private String key;
    private final Long expireTimeMs = 1000 * 60 * 60L;

    public UserJoinResponse join(String userName, String password) {

        userRepository.findByUserName(userName)
                .ifPresent(user -> {
                    throw new MutsaSNSException(ErrorCode.DUPLICATED_USER_NAME);
                });

        User user = User.builder()
                .userName(userName)
                .password(encoder.encode(password))
                .build();
        userRepository.save(user);

        return new UserJoinResponse(user.getUserId(), user.getUserName());
    }

    public String login(String userName, String password) {

        User selectedUser = userRepository.findByUserName(userName)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.USERNAME_NOT_FOUND));

        if (!encoder.matches(password, selectedUser.getPassword())) {
            throw new MutsaSNSException(ErrorCode.INVALID_PASSWORD);
        }

        String token = JwtTokenUtil.createToken(selectedUser.getUserName(), key, expireTimeMs);
        log.info("token : {}", token);
        return token;
    }
}