package com.example.mutsasns.service;

import com.example.mutsasns.domain.dto.alarm.AlarmDto;
import com.example.mutsasns.domain.entity.User;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.repository.AlarmRepository;
import com.example.mutsasns.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AlarmService {

    private final UserRepository userRepository;
    private final AlarmRepository alarmRepository;

    public Page<AlarmDto> alarm(Pageable pageable, String userName) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new MutsaSNSException(ErrorCode.USERNAME_NOT_FOUND));

        return alarmRepository.findAllByUser(pageable, user).map(AlarmDto::of);
    }
}