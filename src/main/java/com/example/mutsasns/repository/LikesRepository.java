package com.example.mutsasns.repository;

import com.example.mutsasns.domain.entity.Likes;
import com.example.mutsasns.domain.entity.Post;
import com.example.mutsasns.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LikesRepository extends JpaRepository<Likes, Long> {
    Optional<Likes> findByPostAndUser(Post post, User user);

    Integer countByPost(Post post);
}
