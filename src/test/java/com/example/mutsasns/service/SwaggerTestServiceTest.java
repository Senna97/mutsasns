package com.example.mutsasns.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SwaggerTestServiceTest {

    SwaggerTestService swaggerTestService = new SwaggerTestService();

    @Test
    void sumOfDigit_success() {
        assertEquals("21", swaggerTestService.sumOfDigit(687));
    }
}