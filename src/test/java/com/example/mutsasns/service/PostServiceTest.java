package com.example.mutsasns.service;

import com.example.mutsasns.domain.dto.post.PostDto;
import com.example.mutsasns.domain.dto.post.PostRequest;
import com.example.mutsasns.domain.entity.Post;
import com.example.mutsasns.domain.entity.User;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.repository.PostRepository;
import com.example.mutsasns.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class PostServiceTest {
    private final UserRepository userRepository = Mockito.mock(UserRepository.class);
    private final PostRepository postRepository = Mockito.mock(PostRepository.class);
    private final PostService postService = new PostService(userRepository, postRepository);

    private final Long userId = 1L;
    private final String userName = "userName";
    private final String password = "password";
    private final User user = User.builder()
            .userId(userId)
            .userName(userName)
            .password(password)
            .build();

    private final Long anotherUserId = 2L;
    private final String anotherUserName = "userName2";
    private final String anotherPassword = "password2";
    private final User anotherUser = User.builder()
            .userId(anotherUserId)
            .userName(anotherUserName)
            .password(anotherPassword)
            .build();

    private final Long postId = 1L;
    private final String title = "title";
    private final String body = "body";
    private final Post post = Post.builder()
            .postId(postId)
            .title(title)
            .body(body)
            .user(user)
            .build();

    /* ========== 포스트 등록 ========== */
    @Test
    @DisplayName("포스트 등록 성공")
    void create_success() {
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.of(user));
        when(postRepository.save(any()))
                .thenReturn(post);

        Assertions.assertDoesNotThrow(() -> postService.create(new PostRequest(title, body), userName));
    }

    @Test
    @DisplayName("포스트 등록 실패 - 로그인 하지 않은 경우")
    void create_fail() {
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.empty());
        when(postRepository.save(any()))
                .thenReturn(post);

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class, () -> postService.create(new PostRequest(title, body), userName));
        assertEquals(ErrorCode.INVALID_PERMISSION, mutsaSNSException.getErrorCode());
    }

    /* ========== 포스트 조회 ========== */
    @Test
    @DisplayName("포스트 상세 조회 성공")
    void readById_success() {
        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));

        PostDto postDto = postService.readById(postId);
        assertEquals(postDto.getUserName(), userName);
    }

    /* ========== 포스트 수정 ========== */
    @Test
    @DisplayName("포스트 수정 실패 - 해당 포스트가 없는 경우")
    void update_fail1() {
        when(postRepository.findById(postId))
                .thenReturn(Optional.empty());

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class, () -> postService.update(postId, new PostRequest(title, body), userName));
        assertEquals(ErrorCode.POST_NOT_FOUND, mutsaSNSException.getErrorCode());
    }

    @Test
    @DisplayName("포스트 수정 실패 - 해당 유저가 없는 경우")
    void update_fail2() {
        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.empty());

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class, () -> postService.update(postId, new PostRequest(title, body), userName));
        assertEquals(ErrorCode.USERNAME_NOT_FOUND, mutsaSNSException.getErrorCode());
    }

    @Test
    @DisplayName("포스트 수정 실패 - 작성자와 유저가 일치하지 않는 경우")
    void update_fail3() {
        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.of(user));
        when(userRepository.findByUserName(anotherUserName))
                .thenReturn(Optional.of(anotherUser));

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(
                MutsaSNSException.class, () -> postService.update(postId, new PostRequest(title, body), anotherUserName));
        assertEquals(ErrorCode.INVALID_PERMISSION, mutsaSNSException.getErrorCode());
    }

    /* ========== 포스트 삭제 ========== */
    @Test
    @DisplayName("포스트 삭제 실패 - 해당 포스트가 없는 경우")
    void delete_fail1() {
        when(postRepository.findById(postId))
                .thenReturn(Optional.empty());

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class, () -> postService.delete(postId, userName));
        assertEquals(ErrorCode.POST_NOT_FOUND, mutsaSNSException.getErrorCode());
    }

    @Test
    @DisplayName("포스트 삭제 실패 - 해당 유저가 없는 경우")
    void delete_fail2() {
        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.empty());

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class, () -> postService.delete(postId, userName));
        assertEquals(ErrorCode.USERNAME_NOT_FOUND, mutsaSNSException.getErrorCode());
    }

    @Test
    @DisplayName("포스트 삭제 실패 - 작성자와 유저가 일치하지 않는 경우")
    void delete_fail3() {
        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.of(user));
        when(userRepository.findByUserName(anotherUserName))
                .thenReturn(Optional.of(anotherUser));

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class, () -> postService.delete(postId , anotherUserName));
        assertEquals(ErrorCode.INVALID_PERMISSION, mutsaSNSException.getErrorCode());
    }
}