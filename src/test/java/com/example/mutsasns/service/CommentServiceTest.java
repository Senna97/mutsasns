package com.example.mutsasns.service;

import com.example.mutsasns.domain.dto.comment.CommentRequest;
import com.example.mutsasns.domain.entity.Comment;
import com.example.mutsasns.domain.entity.Post;
import com.example.mutsasns.domain.entity.User;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.repository.AlarmRepository;
import com.example.mutsasns.repository.CommentRepository;
import com.example.mutsasns.repository.PostRepository;
import com.example.mutsasns.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class CommentServiceTest {
    private final UserRepository userRepository = Mockito.mock(UserRepository.class);
    private final PostRepository postRepository = Mockito.mock(PostRepository.class);
    private final CommentRepository commentRepository = Mockito.mock(CommentRepository.class);
    private final AlarmRepository alarmRepository = Mockito.mock(AlarmRepository.class);
    private final CommentService commentService = new CommentService(userRepository, postRepository, commentRepository, alarmRepository);

    private final Long userId = 1L;
    private final String userName = "userName";
    private final String password = "password";
    private final User user = User.builder()
            .userId(userId)
            .userName(userName)
            .password(password)
            .build();

    private final Long anotherUserId = 2L;
    private final String anotherUserName = "userName2";
    private final String anotherPassword = "password2";
    private final User anotherUser = User.builder()
            .userId(anotherUserId)
            .userName(anotherUserName)
            .password(anotherPassword)
            .build();

    private final Long postId = 1L;
    private final String title = "title";
    private final String body = "body";
    private final Post post = Post.builder()
            .postId(postId)
            .title(title)
            .body(body)
            .user(user)
            .build();

    private final Long commentId = 1L;
    private final String comment = "comment";
    private final Comment commentEX = Comment.builder()
            .commentId(commentId)
            .comment(comment)
            .user(user)
            .post(post)
            .build();

    /* ========== 댓글 수정 ========== */
    @Test
    @DisplayName("댓글 수정 실패 - 유저가 존재하지 않는 경우")
    void update_fail1() {
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.empty());

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class,
                () -> commentService.update(postId, commentId, userName, new CommentRequest(comment)));
        assertEquals(ErrorCode.USERNAME_NOT_FOUND, mutsaSNSException.getErrorCode());
    }

    @Test
    @DisplayName("댓글 수정 실패 - 게시물이 존재하지 않는 경우")
    void update_fail2() {
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.of(user));
        when(postRepository.findById(postId))
                .thenReturn(Optional.empty());

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class,
                () -> commentService.update(postId, commentId, userName, new CommentRequest(comment)));
        assertEquals(ErrorCode.POST_NOT_FOUND, mutsaSNSException.getErrorCode());
    }

    @Test
    @DisplayName("댓글 수정 실패 - 댓글이 존재하지 않는 경우")
    void update_fail3() {
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.of(user));
        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));
        when(commentRepository.findById(commentId))
                .thenReturn(Optional.empty());

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class,
                () -> commentService.update(postId, commentId, userName, new CommentRequest(comment)));
        assertEquals(ErrorCode.COMMENT_NOT_FOUND, mutsaSNSException.getErrorCode());
    }

    @Test
    @DisplayName("댓글 수정 실패 - 작성자와 유저가 일치하지 않는 경우")
    void update_fail4() {
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.of(user));
        when(userRepository.findByUserName(anotherUserName))
                .thenReturn(Optional.of(anotherUser));
        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));
        when(commentRepository.findById(commentId))
                .thenReturn(Optional.of(commentEX));

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class,
                () -> commentService.update(postId, commentId, anotherUserName, new CommentRequest(comment)));
        assertEquals(ErrorCode.INVALID_PERMISSION, mutsaSNSException.getErrorCode());
    }

    /* ========== 댓글 삭제 ========== */
    @Test
    @DisplayName("댓글 삭제 실패 - 유저가 존재하지 않는 경우")
    void delete_fail1() {
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.empty());

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class,
                () -> commentService.delete(postId, commentId, userName));
        assertEquals(ErrorCode.USERNAME_NOT_FOUND, mutsaSNSException.getErrorCode());
    }

    @Test
    @DisplayName("댓글 삭제 실패 - 게시물이 존재하지 않는 경우")
    void delete_fail2() {
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.of(user));
        when(postRepository.findById(postId))
                .thenReturn(Optional.empty());

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class,
                () -> commentService.delete(postId, commentId, userName));
        assertEquals(ErrorCode.POST_NOT_FOUND, mutsaSNSException.getErrorCode());
    }

    @Test
    @DisplayName("댓글 삭제 실패 - 댓글이 존재하지 않는 경우")
    void delete_fail3() {
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.of(user));
        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));
        when(commentRepository.findById(commentId))
                .thenReturn(Optional.empty());

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class,
                () -> commentService.delete(postId, commentId, userName));
        assertEquals(ErrorCode.COMMENT_NOT_FOUND, mutsaSNSException.getErrorCode());
    }

    @Test
    @DisplayName("댓글 삭제 실패 - 작성자와 유저가 일치하지 않는 경우")
    void delete_fail4() {
        when(userRepository.findByUserName(userName))
                .thenReturn(Optional.of(user));
        when(userRepository.findByUserName(anotherUserName))
                .thenReturn(Optional.of(anotherUser));
        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));
        when(commentRepository.findById(commentId))
                .thenReturn(Optional.of(commentEX));

        MutsaSNSException mutsaSNSException = Assertions.assertThrows(MutsaSNSException.class,
                () -> commentService.delete(postId, commentId, anotherUserName));
        assertEquals(ErrorCode.INVALID_PERMISSION, mutsaSNSException.getErrorCode());
    }
}