package com.example.mutsasns.controller;

import com.example.mutsasns.domain.dto.user.UserJoinRequest;
import com.example.mutsasns.domain.dto.user.UserJoinResponse;
import com.example.mutsasns.domain.dto.user.UserLoginRequest;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Autowired
    ObjectMapper objectMapper;

    /* ========== 회원가입 ========== */
    @Test
    @DisplayName("회원가입 성공")
    @WithMockUser
    void join_success() throws Exception {
        // given
        UserJoinRequest userJoinRequest = new UserJoinRequest("userName", "password");
        UserJoinResponse userJoinResponse = new UserJoinResponse(userJoinRequest.getUserId(), userJoinRequest.getUserName());

        // when
        when(userService.join(any(), any()))
                .thenReturn(userJoinResponse);

        // then
        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userJoinRequest)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("회원가입 실패 - userName 중복인 경우")
    @WithMockUser
    void join_fail() throws Exception {
        // given
        UserJoinRequest userJoinRequest = new UserJoinRequest("userName", "password");

        // when
        when(userService.join(any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.DUPLICATED_USER_NAME));

        // then
        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userJoinRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.DUPLICATED_USER_NAME.getHttpStatus().value()));
    }

    /* ========== 로그인 ========== */
    @Test
    @DisplayName("로그인 성공")
    @WithMockUser
    void login_success() throws Exception {
        // given
        UserLoginRequest userLoginRequest = new UserLoginRequest("userName", "password");

        // when
        when(userService.login(any(), any()))
                .thenReturn("token");

        // then
        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userLoginRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").exists())
                .andExpect(jsonPath("$.result.jwt").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("로그인 실패 - userName 없는 경우")
    @WithMockUser
    void login_fail1() throws Exception {
        // given
        UserLoginRequest userLoginRequest = new UserLoginRequest("userName", "password");

        // when
        when(userService.login(any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.USERNAME_NOT_FOUND));

        // then
        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userLoginRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.USERNAME_NOT_FOUND.getHttpStatus().value()));
    }

    @Test
    @DisplayName("로그인 실패 - password 틀린 경우")
    @WithMockUser
    void login_fail2() throws Exception {
        // given
        UserLoginRequest userLoginRequest = new UserLoginRequest("userName", "password");

        // when
        when(userService.login(any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.INVALID_PASSWORD));

        // then
        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userLoginRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PASSWORD.getHttpStatus().value()));
    }
}