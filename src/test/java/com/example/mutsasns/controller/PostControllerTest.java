package com.example.mutsasns.controller;

import com.example.mutsasns.domain.dto.post.PostDto;
import com.example.mutsasns.domain.dto.post.PostRequest;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.service.PostService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PostController.class)
class PostControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    PostService postService;

    @Autowired
    ObjectMapper objectMapper;

    /* ========== 포스트 등록 ========== */
    @Test
    @DisplayName("포스트 등록 성공")
    @WithMockUser
    void create_success() throws Exception {
        // given
        PostRequest postRequest = new PostRequest("title", "body");

        // when
        when(postService.create(any(), any()))
                .thenReturn(PostDto.builder().id(1L).build());

        // then
        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.result.message").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("포스트 등록 실패 - 로그인 하지 않은 경우")
    @WithAnonymousUser
    void create_fail() throws Exception {
        // given
        PostRequest postRequest = new PostRequest("title", "body");

        // when
        when(postService.create(any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.INVALID_PERMISSION));

        // then
        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PERMISSION.getHttpStatus().value()));
    }

    /* ========== 포스트 조회 ========== */
    @Test
    @DisplayName("포스트 상세 조회 성공")
    @WithMockUser
    void readById_success() throws Exception {
        // given
        PostDto postDto = PostDto.builder()
                .id(1L)
                .title("title")
                .body("body")
                .userName("userName")
                .createdAt("yyyy-mm-dd hh:mm:ss")
                .lastModifiedAt("yyyy-mm-dd hh:mm:ss")
                .build();

        // when
        when(postService.readById(any()))
                .thenReturn(postDto);

        // then
        mockMvc.perform(get("/api/v1/posts/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.title").exists())
                .andExpect(jsonPath("$.result.body").exists())
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.createdAt").exists())
                .andExpect(jsonPath("$.result.lastModifiedAt").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("포스트 리스트 조회 성공")
    @WithMockUser
    void readAll_success() throws Exception {

        mockMvc.perform(get("/api/v1/posts")
                        .param("size", "20")
                        .param("sort", "createdAt, DESC"))
                .andExpect(status().isOk());

        ArgumentCaptor<Pageable> pageableArgumentCaptor = ArgumentCaptor.forClass(Pageable.class);

        verify(postService).readAll(pageableArgumentCaptor.capture());
        PageRequest pageRequest = (PageRequest) pageableArgumentCaptor.getValue();

        assertEquals(20, pageRequest.getPageSize());
        assertEquals(Sort.by("createdAt", "DESC"), pageRequest.withSort(Sort.by("createdAt", "DESC")).getSort());

    }

    /* ========== 포스트 수정 ========== */
    @Test
    @DisplayName("포스트 수정 성공")
    @WithMockUser
    void update_success() throws Exception {
        // given
        PostRequest postRequest = new PostRequest("modified title", "modified body");

        // when
        when(postService.update(any(), any(), any()))
                .thenReturn(PostDto.builder().id(1L).build());

        // then
        mockMvc.perform(put("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.result.message").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("포스트 수정 실패 - 인증에 실패한 경우")
    @WithAnonymousUser
    void update_fail1() throws Exception {
        // given
        PostRequest postRequest = new PostRequest("modified title", "modified body");

        // when
        when(postService.update(any(), any(), any()))
                .thenReturn(PostDto.builder().id(1L).build());

        // then
        mockMvc.perform(put("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PERMISSION.getHttpStatus().value()));
    }

    @Test
    @DisplayName("포스트 수정 실패 - 작성자와 유저가 일치하지 않는 경우")
    @WithMockUser
    void update_fail2() throws Exception {
        // given
        PostRequest postRequest = new PostRequest("modified title", "modified body");

        // when
        when(postService.update(any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.INVALID_PERMISSION));

        // then
        mockMvc.perform(put("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PERMISSION.getHttpStatus().value()));
    }

    @Test
    @DisplayName("포스트 수정 실패 - 데이터베이스에 에러가 난 경우")
    @WithMockUser
    void update_fail3() throws Exception {
        // given
        PostRequest postRequest = new PostRequest("modified title", "modified body");

        // when
        when(postService.update(any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.DATABASE_ERROR));

        // then
        mockMvc.perform(put("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.DATABASE_ERROR.getHttpStatus().value()));
    }

    /* ========== 포스트 삭제 ========== */
    @Test
    @DisplayName("포스트 삭제 성공")
    @WithMockUser
    void delete_success() throws Exception {
        // given
        PostDto postDto = PostDto.builder()
                .id(1L)
                .build();

        // when
        when(postService.delete(any(), any()))
                .thenReturn(postDto);

        // then
        mockMvc.perform(delete("/api/v1/posts/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(jsonPath("$.result.message").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("포스트 삭제 실패 - 인증에 실패한 경우")
    @WithAnonymousUser
    void delete_fail1() throws Exception {
        // given
        PostDto postDto = PostDto.builder()
                .id(1L)
                .build();

        // when
        when(postService.delete(any(), any()))
                .thenReturn(postDto);

        // then
        mockMvc.perform(delete("/api/v1/posts/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PERMISSION.getHttpStatus().value()));
    }

    @Test
    @DisplayName("포스트 삭제 실패 - 작성자와 유저가 일치하지 않는 경우")
    @WithMockUser
    void delete_fail2() throws Exception {
        // given

        // when
        when(postService.delete(any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.INVALID_PERMISSION));

        // then
        mockMvc.perform(delete("/api/v1/posts/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PERMISSION.getHttpStatus().value()));
    }

    @Test
    @DisplayName("포스트 삭제 실패 - 데이터베이스에 에러가 난 경우")
    @WithMockUser
    void delete_fail3() throws Exception {
        // given

        // when
        when(postService.delete(any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.DATABASE_ERROR));

        // then
        mockMvc.perform(delete("/api/v1/posts/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is(ErrorCode.DATABASE_ERROR.getHttpStatus().value()));
    }

    /* ========== 마이 피드 조회 ========== */
    @Test
    @DisplayName("마이 피드 조회 성공")
    @WithMockUser
    void myFeed_success() throws Exception {
        // when
        when(postService.myFeed(any(), any()))
                .thenReturn(Page.empty());

        // then
        mockMvc.perform(get("/api/v1/posts/my")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("마이 피드 조회 실패 - 로그인 하지 않은 경우")
    @WithAnonymousUser
    void myFeed_fail() throws Exception {
        // when
        when(postService.myFeed(any(), any()))
                .thenReturn(Page.empty());

        // then
        mockMvc.perform(get("/api/v1/posts/my")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}