package com.example.mutsasns.controller;

import com.example.mutsasns.domain.Role;
import com.example.mutsasns.domain.dto.comment.CommentDto;
import com.example.mutsasns.domain.dto.comment.CommentRequest;
import com.example.mutsasns.domain.dto.comment.CommentUpdateResponse;
import com.example.mutsasns.domain.entity.Post;
import com.example.mutsasns.domain.entity.User;
import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.service.CommentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CommentController.class)
class CommentControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CommentService commentService;

    @Autowired
    ObjectMapper objectMapper;

    /* ========== 댓글 작성 ========== */
    @Test
    @DisplayName("댓글 작성 성공")
    @WithMockUser
    void create_success() throws Exception {
        //given
        CommentRequest commentRequest = new CommentRequest("comment");
        CommentDto commentDto = CommentDto.builder()
                .id(1L)
                .comment(commentRequest.getComment())
                .userName("userName")
                .postId(1L)
                .createdAt("yyyy-mm-dd hh:mm:ss")
                .build();

        //when
        when(commentService.create(any(), any(), any()))
                .thenReturn(commentDto);

        // then
        mockMvc.perform(post("/api/v1/posts/1/comments")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.comment").exists())
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(jsonPath("$.result.createdAt").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("댓글 작성 실패 - 로그인 하지 않은 경우")
    @WithAnonymousUser
    void create_fail1() throws Exception {
        //given
        CommentRequest commentRequest = new CommentRequest("comment");

        //when
        when(commentService.create(any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.INVALID_PERMISSION));

        // then
        mockMvc.perform(post("/api/v1/posts/1/comments")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("댓글 작성 실패 - 게시물이 존재하지 않는 경우")
    @WithMockUser
    void create_fail2() throws Exception {
        //given
        CommentRequest commentRequest = new CommentRequest("comment");

        //when
        when(commentService.create(any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        // then
        mockMvc.perform(post("/api/v1/posts/1/comments")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.POST_NOT_FOUND.getHttpStatus().value()));
    }

    /* ========== 댓글 조회 ========== */
    @Test
    @DisplayName("댓글 조회 성공")
    @WithMockUser
    void read_success() throws Exception {
        // when
        when(commentService.read(any(), any()))
                .thenReturn(Page.empty());

        // then
        mockMvc.perform(get("/api/v1/posts/1/comments")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /* ========== 댓글 수정 ========== */
    @Test
    @DisplayName("댓글 수정 성공")
    @WithMockUser
    void update_success() throws Exception {
        // given
        CommentRequest commentRequest = new CommentRequest("comment");
        User user = new User(1L, "userName", "password", Role.USER);
        Post post = new Post(1L, user, "title", "body", false);
        CommentUpdateResponse commentUpdateResponse = CommentUpdateResponse.builder()
                .id(1L)
                .comment(commentRequest.getComment())
                .userName(user.getUserName())
                .postId(post.getPostId())
                .createdAt("yyyy-mm-dd hh:mm:ss")
                .lastModifiedAt("yyyy-mm-dd hh:mm:ss")
                .build();

        // when
        when(commentService.update(any(), any(), any(), any()))
                .thenReturn(commentUpdateResponse);

        // then
        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.comment").exists())
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(jsonPath("$.result.createdAt").exists())
                .andExpect(jsonPath("$.result.lastModifiedAt").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("댓글 수정 실패 - 인증에 실패한 경우")
    @WithAnonymousUser
    void update_fail1() throws Exception {
        // given
        CommentRequest commentRequest = new CommentRequest("comment");

        // when
        when(commentService.update(any(), any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.INVALID_PERMISSION));

        // then
        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("댓글 수정 실패 - 게시물이 존재하지 않는 경우")
    @WithMockUser
    void update_fail2() throws Exception {
        // given
        CommentRequest commentRequest = new CommentRequest("comment");

        // when
        when(commentService.update(any(), any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        // then
        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.POST_NOT_FOUND.getHttpStatus().value()));
    }

    @Test
    @DisplayName("댓글 수정 실패 - 작성자와 유저가 일치하지 않는 경우")
    @WithMockUser
    void update_fail3() throws Exception {
        // given
        CommentRequest commentRequest = new CommentRequest("comment");

        // when
        when(commentService.update(any(), any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.INVALID_PERMISSION));

        // then
        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PERMISSION.getHttpStatus().value()));
    }

    @Test
    @DisplayName("댓글 수정 실패 - 데이터베이스에 에러가 난 경우")
    @WithMockUser
    void update_fail4() throws Exception {
        // given
        CommentRequest commentRequest = new CommentRequest("comment");

        // when
        when(commentService.update(any(), any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.DATABASE_ERROR));

        // then
        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.DATABASE_ERROR.getHttpStatus().value()));
    }

    /* ========== 댓글 삭제 ========== */
    @Test
    @DisplayName("댓글 삭제 성공")
    @WithMockUser
    void delete_success() throws Exception {
        // given
        CommentDto commentDto = CommentDto.builder()
                .id(1L)
                .build();

        // when
        when(commentService.delete(any(), any(), any()))
                .thenReturn(commentDto);

        // then
        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(jsonPath("$.result.message").exists())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("댓글 삭제 실패 - 인증에 실패한 경우")
    @WithAnonymousUser
    void delete_fail1() throws Exception {
        // when
        when(commentService.delete(any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.INVALID_PERMISSION));

        // then
        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("댓글 삭제 실패 - 게시물이 존재하지 않는 경우")
    @WithMockUser
    void delete_fail2() throws Exception {
        // when
        when(commentService.delete(any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        // then
        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is(ErrorCode.POST_NOT_FOUND.getHttpStatus().value()));
    }

    @Test
    @DisplayName("댓글 삭제 실패 - 작성자와 유저가 일치하지 않는 경우")
    @WithMockUser
    void delete_fail3() throws Exception {
        // when
        when(commentService.delete(any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.INVALID_PERMISSION));

        // then
        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PERMISSION.getHttpStatus().value()));
    }

    @Test
    @DisplayName("댓글 삭제 실패 - 데이터베이스에 에러가 난 경우")
    @WithMockUser
    void delete_fail4() throws Exception {
        // when
        when(commentService.delete(any(), any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.DATABASE_ERROR));

        // then
        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is(ErrorCode.DATABASE_ERROR.getHttpStatus().value()));
    }
}