package com.example.mutsasns.controller;

import com.example.mutsasns.exception.ErrorCode;
import com.example.mutsasns.exception.MutsaSNSException;
import com.example.mutsasns.service.LikesService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LikesController.class)
class LikesControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    LikesService likesService;

    @Test
    @DisplayName("좋아요 누르기 성공")
    @WithMockUser
    void doLikes_success() throws Exception {
        // when
        when(likesService.doLikes(any(), any()))
                .thenReturn("좋아요를 눌렀습니다.");

        // then
        mockMvc.perform(post("/api/v1/posts/1/likes")
                        .with(csrf()))
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").exists())
                .andExpect(jsonPath("$.result").exists())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("좋아요 누르기 실패 - 로그인 하지 않은 경우")
    @WithAnonymousUser
    void doLikes_fail1() throws Exception {
        // when
        when(likesService.doLikes(any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.INVALID_PERMISSION));

        // then
        mockMvc.perform(post("/api/v1/posts/1/likes")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("좋아요 누르기 실패 - 게시물이 존재하지 않는 경우")
    @WithMockUser
    void doLikes_fail2() throws Exception {
        // when
        when(likesService.doLikes(any(), any()))
                .thenThrow(new MutsaSNSException(ErrorCode.POST_NOT_FOUND));

        // then
        mockMvc.perform(post("/api/v1/posts/1/likes")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}